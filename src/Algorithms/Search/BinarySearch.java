// Title        : Binary Search Implementation
// Purpose      : To Implement and test Binary Search Algorithm

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191112     SHPRLK              Placed the Class into sub package Search.
// 20191111     SHPRLK              Modified the WHILE loop condition from || operator to && operator to solve the
//                                  issue in exiting searches where the value doesn't exist in the array.
// 20191110     SHPRLK              Implemented the Binary Search Class.
// 20191030     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------

package Algorithms.Search;

public class BinarySearch {
    private int[]   array_to_search_;                 // inbound array to be search
    private int[]   sorted_array_;                    // if the inbound array is not sorted then the sorted array
    private int     array_length_ ;                    // size of the array to be dealt within this scope
    private int     value_to_search_    = 0;
    private boolean value_found_        = false;
    int array_start_index_              = 0 ;
    int array_end_index_                = array_length_-1;
    int array_mid_index_                = 0 ;
    int search_result_index_            = -9999;

    public BinarySearch(int[] incoming_array_to_search_ , int incoming_value_to_search_ ){
        this.array_to_search_   = incoming_array_to_search_;
        this.value_to_search_   = incoming_value_to_search_;
        this.array_length_      = array_to_search_.length;
        this.array_start_index_  = 0 ;
        this.array_end_index_    = array_length_;
        this.array_mid_index_    = 0 ;
        search_result_index_     =  DoBinarySearch();

        if(search_result_index_< 0){
            System.out.println("The Search value " + value_to_search_ + " is not found in the array");
        }
        else {
            System.out.println("The Search value " + value_to_search_  + "is found at index: " + search_result_index_);
        }
    }

    public int DoBinarySearch(){
//        if(!CheckArraySorted()){                  // if inbound array is not sorted then have to sort it prior to search
//            // array sort
//        }
            while((array_start_index_> array_end_index_) && (!value_found_)){

                array_mid_index_ = (array_start_index_ + array_end_index_)/2;

                if(array_to_search_[array_mid_index_] == value_to_search_){
                    value_found_ = true;
                    return  array_mid_index_;
                }
                else{
                    if(array_to_search_[array_mid_index_] < value_to_search_){
                        array_start_index_ = array_mid_index_;
                    }
                    else {
                        array_end_index_ = array_mid_index_;
                    }
                }
            }
            return -9999;
    }
}
