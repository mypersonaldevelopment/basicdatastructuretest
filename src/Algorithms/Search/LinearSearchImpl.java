// Title        : Linear Search Implementation
// Purpose      : To Implement and test Linear Search Algorithm
// Comments     : Linear Search is the most straight forward algorithm

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191117     SHPRLK              Created the file.
// ---------------------------------------------------------------------------------------------------------------------

package Algorithms.Search;

public class LinearSearchImpl {
    private int[]   array_to_search_;                 // inbound array to be search
    private int     array_length_ ;
    private int     value_to_search_        = 0;
    private  int    index_of_search_result_ = -9999;

    public LinearSearchImpl(int[] incoming_array_to_search_ , int incoming_value_to_search_ ) {
        this.array_to_search_   = incoming_array_to_search_;
        this.value_to_search_   = incoming_value_to_search_;
        this.array_length_      = array_to_search_.length;
        this.index_of_search_result_ = DoLinearSearch();
    }

    public int DoLinearSearch() {
        for (int array_traversal_index_ = 0; array_traversal_index_ < array_length_; array_traversal_index_++) {
            if (array_to_search_[array_traversal_index_] == value_to_search_) {
                return array_traversal_index_;
            }
        }
        return -9999;
    }
    public void ShowResult(){
        if (index_of_search_result_ < 0){
            System.out.println(" The value " + value_to_search_ + " does not exist in the array");
        }
        else {
            System.out.println(" The value " + value_to_search_ + " ists in the array at index : " + index_of_search_result_);
        }
    }
}
