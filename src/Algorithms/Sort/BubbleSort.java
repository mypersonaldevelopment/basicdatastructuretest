// Title        : Bubble Sort
// Purpose      : To Implement and test Bubble Sort
// Limitation   : THe Bubble sort takes only int arrays as an input

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191114     SHPRLK              Modified PrintArray() to remove unnecessary local variable which is globally available.
// 20191112     SHPRLK              Placed the Class into sub package Sort.
// 20191109     SHPRLK              Implemented with the reference to a pseudo code.
// 20191030     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------

package Algorithms.Sort;

public class BubbleSort {
    private int[] array_to_sort_;                   // inbound array to be sorted
    private int[] sorted_array_;                    // the variable to hold the final sorted array
    private int array_length_ ;                     // size of the array to be dealt within this scope

    public BubbleSort(int[] incoming_array_to_sort_) {
        this.array_length_  = incoming_array_to_sort_.length;           // acquiring the inbound array size
        this.array_to_sort_ = incoming_array_to_sort_;                  // assiging the inbound array to the initialized array
    }

    public void RunSort(boolean display_arrays_){                           // Executing the sort
        if (display_arrays_) {                                              // Displaying if flags are set to true
            PrintArrayToSort();
        }

        int temp_value_holder_  = 0;                                    // variable to hold the value during swapping
        for (int main_lopp_index_ = array_length_; main_lopp_index_ >=0 ; main_lopp_index_-- ){          // the loop to iterate in reverse to bubble up the highest number
            for (int comparator_loop_index_ = 0; comparator_loop_index_ < (main_lopp_index_ - 1) ; comparator_loop_index_++){
                if( array_to_sort_[comparator_loop_index_] > array_to_sort_[comparator_loop_index_+1] ){
                    temp_value_holder_                        = array_to_sort_[comparator_loop_index_];
                    array_to_sort_[comparator_loop_index_]    = array_to_sort_[comparator_loop_index_+1];
                    array_to_sort_[comparator_loop_index_+1]  = temp_value_holder_;
                }
            }
        }
        sorted_array_ = array_to_sort_;


        if (display_arrays_) {                                          // Displaying if flags are set to true
            PrintSortedArray();
        }
    }

    public void RunSort(){                                  // Overloaded method to not have nay input parameters to display values
        RunSort(false);;
    }

    public int[] GetSortedArray(){                          // return the sorted array
        return  sorted_array_;
    }

    public void PrintArrayToSort(){                         // Print the array to be sorted
        System.out.print("Array to be sorted    : ");
        PrintArray(array_to_sort_);;
        System.out.println();
    }

    public void PrintSortedArray(){                         //// Print the sorted array
        System.out.print("Sorted Array          : ");
        PrintArray(sorted_array_);
        System.out.println();
    }

    public void PrintArray(int [] array_to_print){          // print any array that is passed into it
        for(int array_index_ = 0 ; array_index_< array_length_; array_index_++){
            System.out.print(" | " + array_to_print[array_index_]);;
        }
        System.out.print("|");;
    }


}
