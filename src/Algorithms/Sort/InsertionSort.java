// Title        : Insertion Sort
// Purpose      : To Implement and test Insertion Sort
// Limitation   : The Insertion sort takes only int arrays as an input

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191114     SHPRLK              Created the file and implemented the Insertion Sort.
// ---------------------------------------------------------------------------------------------------------------------

package Algorithms.Sort;

import sun.awt.windows.WPrinterJob;

public class InsertionSort {
    private int[] array_to_sort_;                   // inbound array to be sorted
    private int[] sorted_array_;                    // the variable to hold the final sorted array
    private int array_length_ ;                     // size of the array to be dealt within this scope

    public InsertionSort(int[] incoming_array_to_sort_) {
        this.array_length_  = incoming_array_to_sort_.length;           // acquiring the inbound array size
        this.array_to_sort_ = incoming_array_to_sort_;                  // assiging the inbound array to the initialized array
        PrintArray(array_to_sort_);;
    }

    public void RunSort(){                                              // Executing the sort
        int temp_Swap_value_holder = 0;
        for(int array_traversal_index_ = 1; array_traversal_index_ < array_length_; array_traversal_index_++){
            for( int array_comparison_loop_index_ = 0; array_comparison_loop_index_ < array_traversal_index_; array_comparison_loop_index_++){
                if( array_to_sort_[array_comparison_loop_index_] > array_to_sort_[array_traversal_index_]){
                    temp_Swap_value_holder = array_to_sort_[array_traversal_index_];
                    for (int swap_loop_index_ = array_traversal_index_; swap_loop_index_> array_comparison_loop_index_ ; swap_loop_index_--){
                        array_to_sort_[swap_loop_index_] = array_to_sort_[swap_loop_index_-1];
                    }
                    array_to_sort_[array_comparison_loop_index_] = temp_Swap_value_holder;
                }
            }
        }
        sorted_array_ = array_to_sort_;
        PrintArray(sorted_array_);;
    }

    public void PrintArray(int [] array_to_print){          // print any array that is passed into it
        System.out.println();
        for(int array_index_ = 0 ; array_index_< array_length_; array_index_++){
            System.out.print(" | " + array_to_print[array_index_]);;
        }
        System.out.print("|");;
    }
}
