// Title        : Stack Implementation
// Purpose      : To Implement and test Stack Data Structure
// Limitation   : THe stack only works as an int stack

//  DATE        CONTRIBUTOR ID      COMMENT
//  ---------   --------------      ------------------------------------------------------------------------------------
//  20191131    SHPRLK              Tested Binary Search using Binary Runner Classes.
//  20191030    SHPRLK              Created the file and tested out the Stack Implementation through the Stack Runner.
//  20200209    SHPRLK              Added Binary Tree check to main method.
// ---------------------------------------------------------------------------------------------------------------------

package com.company;

import RunnerPackage.*;

public class Main {
    public static void main(String[] args) {
//        StackRunner stack_test_1_ = new StackRunner();
//        BubbleSortRunner bsr1_ = new BubbleSortRunner();
//        BinarySearchRunner bsr = new BinarySearchRunner();
//        QueueRunner q1_ = new QueueRunner();
//        InsertionSortRunner isr_1_ = new InsertionSortRunner();
        BinaryTreeRunner bnr1 = new BinaryTreeRunner();

    }
}
