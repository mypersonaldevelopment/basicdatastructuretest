// Title        : Stack Implementation
// Purpose      : To Implement and test Stack Data Structure
// Limitation   : THe stack only works as an int stack


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191030     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------



package DataStructures;

public class StackImpl {                    // Stack class

    private int stack_size_ = 0;
    private int top_index_  = 0;
    private int stack_array_[];

    public StackImpl(int statck_size){      // Stack Constructor only needing the stack size as the input
         this.stack_size_  = statck_size;
         this.stack_array_ = new int[this.stack_size_];
         System.out.println(("You have created a stack of size " + stack_size_));
    }

    public void Push(int value_){           // Basic function to insert data from the top
        if(StackIsFull()){
            System.out.println("The stack is full, cannot insert further values, inclding the current one : "+ value_);
            System.exit(0);
            return;

        }
        else{
            this.stack_array_[top_index_] = value_;
            top_index_++;
            PrintStack("Push");;
        }
    }

    public void Pop(){            // Basic function to remove data from the top
        if(StackIsEmpty()){
            System.out.println("The stack is Empty, There is nothing to remove");
            System.exit(0);
        }
        else{
            top_index_--;
            this.stack_array_[top_index_] = 0 ;
            PrintStack("Pop");;
        }
    }

    public boolean StackIsFull() {          // Check if the stack is already full for it's given limit
        if (this.stack_size_  == (this.top_index_ )) {
            return true;
        } else {
            return false;
        }
    }

    public boolean StackIsEmpty(){          // Check if the stack consists any data
        if(this.top_index_== 0){
            return  true;
        }
        else{
            return false;
        }
    }

    public int Peek(){                      // Check the data that is at the top
        if(!StackIsEmpty()){
            return  this.stack_array_[this.top_index_];
        }
        else{
            System.out.println("There is nothing to Peek here");
            return  -1;
        }
    }

    public void PrintStack(){
        int count_ = 0;
        while(count_ < this.stack_size_){
            System.out.print(" | " + this.stack_array_[count_]);
            count_++;
        }
        System.out.println(" |");;
    }

    public void PrintStack(String from_value_){         // Chained methods if the source of printing has to be printed
        System.out.println("-----------"+from_value_+"------------- ");;
        PrintStack();
        System.out.println("-----------"+from_value_+"----done----- ");;
    }
}



