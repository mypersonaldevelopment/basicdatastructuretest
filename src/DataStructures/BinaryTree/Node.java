// Title        : Node for Binary tree Implementation
// Purpose      : A basic node for Binary tree implementation

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200208     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------


package DataStructures.BinaryTree;

public class Node {
    int key_;
    String name_;
    Node leftNode_;
    Node rightNode_;

    public Node(int key_, String name_){                                        // Initializing a Node with just key and name values
        this.key_ = key_;
        this.name_ = name_;
    }

    public  Node(int key_, String name_, Node leftNode_, Node rightNode_){      // Allowing the creation with full Node properties
        this(key_, name_);
        this.leftNode_= leftNode_;
        this.rightNode_ = rightNode_;
    }
}
