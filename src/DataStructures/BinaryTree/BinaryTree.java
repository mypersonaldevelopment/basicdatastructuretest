// Title        : Binary tree Implementation
// Purpose      : To test and implement Binary tree

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200208     SHPRLK              Created the file
// 20200209     SHPRLK              Corrected the issues in the AddNoneRootNode() to make insertion work.
// ---------------------------------------------------------------------------------------------------------------------

package DataStructures.BinaryTree;

import javax.swing.*;

public class BinaryTree {
    private  Node BtRootNode_;
    private  Node focusNode;                        // temporary node to set focus and process further down the tree

    public Node getRootNode_() {
        return this.BtRootNode_;
    }

    public Node getFocusNode() {
        return this.BtRootNode_;
    }


    public BinaryTree(Node root_){                  //Facilitating the initialization of Binary with a root node itself
        this.BtRootNode_ = root_;
    }

    public BinaryTree(){                            // initializing of binary tree without a root node and then add nodes
        this.BtRootNode_ = null;
    }

    public void AddNodeByValue(int key_, String name_){     // add node by directing passing value to the new Node instead of Node object itself
        Node newNode_ = new Node(key_, name_);
        AddNode( newNode_);;
    }

    public boolean AddRootNode(Node newNode_) {             // If there is no root node add it
        if(this.BtRootNode_== null){
            this.BtRootNode_ = newNode_;
            this.focusNode = this.BtRootNode_;
            PrintNodeProperties(newNode_);
            return true;
        }
        return  false;
    }

    public void AddNoneRootNode( Node rootNode, Node newNode_){
        this.focusNode = rootNode;
        if (focusNode.key_ < newNode_.key_){
            if(focusNode.rightNode_ == null){
                focusNode.rightNode_ = newNode_;
                PrintNodeProperties(focusNode.rightNode_);
                return;
            }
            else {
                AddNoneRootNode(focusNode.rightNode_, newNode_);
            }
        }
        else
        {
            if(focusNode.leftNode_ == null){
                focusNode.leftNode_ = newNode_;
                PrintNodeProperties(focusNode.leftNode_ );
                return;
            }
            else
                AddNoneRootNode(focusNode.leftNode_, newNode_);
        }
    }

    public void AddNode(Node newNode_){         // Add further nodes after checking if root is instantiated in the first place
        this.focusNode = null;                  // Setting focusNode to null to ensure new entries go from the root of tree
        if(!AddRootNode( newNode_ )){           // Check if the root node is set, if not set root
            AddNoneRootNode( BtRootNode_, newNode_);    // if root node already exists, proceed to find the suitable node to place it
        }
    }

    public void PrintNodeProperties(Node node_){
        System.out.println(" Node "+ node_.key_ + "  Value : "+ node_.name_);
    }
    }


