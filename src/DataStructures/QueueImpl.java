// Title        : Queue Implementation
// Purpose      : To Implement and test Queue Data Structure
// Limitation   : THe Queue takes only int values as an input

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191114     SHPRLK              Corrected end scenarios with Enqueue and Dequeue by implementing methods to identify queue statuses.
// 20191114     SHPRLK              Corrected the index issue by setting the starting value of end_pointer_ as -1 and placing the
//              SHPRLK              incrementer at the top of Enqueue and decrement operator at the end Dequeue.
// 20191112     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------

package DataStructures;

public class QueueImpl {
    int[] queue_array_ ;
    int queue_size_;
    int end_pointer_;

    public QueueImpl(int intiazlizing_queue_size_){
        this.queue_size_    = intiazlizing_queue_size_;
        this.queue_array_   = new int[queue_size_];
        end_pointer_        = -1;                       // This consists the array index of the queue that holds an inserted value
                                                        // -1 is essential th the working of this incrementer decrementing patterns
    }

    public void Enqueue(int value_to_enqueue_){         // To insert the value that is at the very beginning of the queue (the tail)
        if(QueueIsFull(false)){
            System.out.println("Cannot Insert value : "+ value_to_enqueue_ + " as the queue is full");
        }
        else{
            end_pointer_ = end_pointer_ + 1;
            for(int queue_array_index_ = (end_pointer_); queue_array_index_ > 0; queue_array_index_-- ){
                queue_array_[queue_array_index_] = queue_array_[queue_array_index_-1];
            }
            queue_array_[0] = value_to_enqueue_;
        }
    }

    public void Dequeue(){                      // The method do remove the value at the head(the oldest record at the head of queue)
        if(!QueueIsEmpty()){
            queue_array_[end_pointer_] = 0;
            end_pointer_--;
        }
        else
        {
            System.out.println("There are no more values in the queue to be removed");
        }
    }

    public void ShowQueue(){                    // To print the queue whenever required
        for(int queue_index_ = 0 ; queue_index_ < queue_size_; queue_index_++){
            System.out.print(" | "+ queue_array_[queue_index_]);
        }
        System.out.print(" | ");
        System.out.println();
    }

    public boolean QueueIsFull(boolean print_message_flag_){              // Method to check whether the queue is full
        if(end_pointer_  == (queue_size_-1)){
            if(print_message_flag_){
                System.out.println(" The queue is full ");
            }
            return true;
        }
        return false;
    }
    public boolean QueueIsFull(){                                           // Overloaded method to show message and check whether the queue is full
        return QueueIsFull(true);
    }

    public boolean QueueIsEmpty(boolean print_message_flag_){              // Method to check whether the queue is empty
        if(end_pointer_  < 0 ){
            System.out.println(" The queue is empty ");
            return true;
        }
        return false;
    }

    public boolean QueueIsEmpty(){                                          // Overloaded method to show message and to check whether the queue is empty
        return QueueIsEmpty(true);
    }

    public int Peek(){                                                      // To return the value of the head of queue 
        if (!QueueIsEmpty()){
            return queue_array_[end_pointer_];
        }
        return -999999999;
    }

}
