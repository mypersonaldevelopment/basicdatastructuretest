// Title        : Bubble Sort Runner file
// Purpose      : To de-clutter the main method in the Main.java all the test run codes have been shifted
//              : Runner Class


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191110     SHPRLK              Made the file and implemented the runner commands
// ---------------------------------------------------------------------------------------------------------------------

package RunnerPackage;

import Algorithms.Sort.BubbleSort;

public class BubbleSortRunner {
    //private int[] array_to_sort_ =  new int[8];
    int[] array_to_sort_ = {81,56,29,75,4,21,32,9};
    public BubbleSortRunner(){
        BubbleSort bs1 = new BubbleSort(array_to_sort_);
        bs1.RunSort(true);;
    }
}
