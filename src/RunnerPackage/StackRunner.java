// Title        : Stack Rynner file
// Purpose      : To de-clutter the main method in the Main.java all the test run codes have been shifted
//              : Runner Class


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191110     SHPRLK              Made the package imports and implemented the Runner file.
// 20191108     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------


package RunnerPackage;

import DataStructures.StackImpl;

public class StackRunner {
    public StackRunner(){
        System.out.println("Stack Creation started..");
        StackImpl stack_1_ = new StackImpl(6);
        stack_1_.Push(5);
        stack_1_.Push(89);
        stack_1_.Push(23);
        stack_1_.Push(54);
        stack_1_.Push(10);
        stack_1_.Push(8);
        stack_1_.PrintStack();
        stack_1_.Pop();
        stack_1_.Push(65);
        stack_1_.PrintStack();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.PrintStack();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.Pop();
        stack_1_.PrintStack();
        stack_1_.Pop();

        stack_1_.PrintStack();
    }

}
