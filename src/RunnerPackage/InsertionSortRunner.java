// Title        : Insertion Sort Runner
// Purpose      : To run Insertion Sort
// Limitation   : THe Insertion sort takes only int arrays as an input

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191030     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------

package RunnerPackage;

import Algorithms.Sort.InsertionSort;

public class InsertionSortRunner {
    public InsertionSortRunner(){
        int[] array_to_sort_ = {5622,321,2,4,0,6,54,95,6,6,6,6,48,91,123,465,1236,84,261};
        InsertionSort is_1_ = new InsertionSort(array_to_sort_);
        is_1_.RunSort();
    }
}
