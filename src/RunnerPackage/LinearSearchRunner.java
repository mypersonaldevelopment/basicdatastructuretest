// Title        : Linear Search Runner
// Purpose      : To de-clutter the main method in the Main.java all the test run codes have been shifted
//              : Runner Class

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191117     SHPRLK              Made the file and implemented the runner commands
// ---------------------------------------------------------------------------------------------------------------------

package RunnerPackage;

import Algorithms.Search.LinearSearchImpl;

public class LinearSearchRunner {
    public LinearSearchRunner(){
        int[] array_to_search_ = {5,94,849,2161,4,684,8911,516,19,8198,19,996,16,16,168,16,16,16151,651651,1549,82};
        int value_to_be_searched_ = 82;
        LinearSearchImpl ls_1_ = new LinearSearchImpl(array_to_search_, value_to_be_searched_);
        ls_1_.ShowResult();
    }
}
