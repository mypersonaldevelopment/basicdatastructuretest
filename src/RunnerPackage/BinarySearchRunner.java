// Title        : Binary Search Runner
// Purpose      : To de-clutter the main method in the Main.java all the test run codes have been shifted
//              : Runner Class

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191110     SHPRLK              Made the file and implemented the runner commands
// ---------------------------------------------------------------------------------------------------------------------


package RunnerPackage;

import Algorithms.Search.BinarySearch;

public class BinarySearchRunner {
    public BinarySearchRunner(){
        int[] array_to_search_ =  {23,45,56,89,110,224,995,1003,4659,7892};
        int value_to_search_    =  7000;
        BinarySearch bs1 = new BinarySearch(array_to_search_,value_to_search_);
//        System.out.println("The value "+ value_to_search_ + " exists in the array on the index : " + bs1.DoBinarySearch());
    }
}
