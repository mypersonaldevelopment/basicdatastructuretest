// Title        : Binary tree Runner file
// Purpose      : To de-clutter the main method in the Main.java all the test run codes have been shifted
//              : Runner Class


// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191110     SHPRLK              Made the file and implemented the runner commands
// ---------------------------------------------------------------------------------------------------------------------

package RunnerPackage;

import DataStructures.BinaryTree.BinaryTree;
import DataStructures.BinaryTree.Node;

public class BinaryTreeRunner {
    public BinaryTreeRunner() {
        BinaryTree bt1 = new BinaryTree();

        Node nd1 = new Node(90, "Ninety");
        Node nd2 = new Node(70, "Seventy");

        bt1.AddNodeByValue(60, "Fisrt Element");
        bt1.AddNodeByValue(80, "Eighty");
        bt1.AddNodeByValue(20, "Twenty");
        bt1.AddNodeByValue(40, "Fourty");
        bt1.AddNode(nd1);
        bt1.AddNode(nd2);


    }
}
