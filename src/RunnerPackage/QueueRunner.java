// Title        : Queue Runner
// Purpose      : To de-clutter the main method in the Main.java all the test run codes have been shifted
//              : Runner Class

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20191114     SHPRLK              Created the file
// ---------------------------------------------------------------------------------------------------------------------

package RunnerPackage;

import DataStructures.QueueImpl;

public class QueueRunner {
    public QueueRunner(){
        QueueImpl q1_ = new QueueImpl(10);
        q1_.ShowQueue();
        q1_.Enqueue(44);
        q1_.Enqueue(34);
        q1_.Enqueue(67);
        q1_.Enqueue(45);
        q1_.Enqueue(92);
        q1_.Enqueue(23);
        q1_.Enqueue(18);
        q1_.Enqueue(3434);
        q1_.Enqueue(3434);
        q1_.Enqueue(3);
        q1_.Enqueue(4);
        q1_.Enqueue(5);
        q1_.Enqueue(6);

        q1_.ShowQueue();

        q1_.Dequeue();
        q1_.Dequeue();
        q1_.Dequeue();
        q1_.Dequeue();
        q1_.Dequeue();

        q1_.ShowQueue();

        q1_.Dequeue();
        q1_.Dequeue();
        q1_.Dequeue();
        q1_.Dequeue();
        q1_.Dequeue();

        q1_.ShowQueue();

        q1_.Dequeue();
        q1_.ShowQueue();
    }
}
